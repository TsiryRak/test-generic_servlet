package models;

import genericServlet.annotations.WebClass;
import genericServlet.annotations.WebUrl;
import genericServlet.util.ModelView;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;

@WebClass
public class Employe {
    private static ArrayList<Employe> list_emp=setListEmp();

    private String nom;
    private String prenoms;
    private Date dateNaissance;
    private String[] diplomes;

    public String getNom() {
        return nom;
    }

    public String getPrenoms() {
        return prenoms;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public String[] getDiplomes() {
        return diplomes;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public void setDiplomes(String[] diplomes) {
        this.diplomes = diplomes;
    }

    public Employe(){

    }

    public Employe(String nom, String prenoms, Date dateNaissance, String[] diplomes) {
        this.nom = nom;
        this.prenoms = prenoms;
        this.dateNaissance = dateNaissance;
        this.diplomes = diplomes;
    }

    private static ArrayList<Employe> setListEmp(){
        ArrayList<Employe> list_emp=new ArrayList<Employe>();
        list_emp.add(new Employe("Rakoto","Jean",Date.valueOf("2002-06-07"),new String[]{"Bacc","License"}));
        list_emp.add(new Employe("Andria","Fitia",Date.valueOf("2002-12-16"),new String[]{"Bacc"}));
        list_emp.add(new Employe("Razafy","Leon",Date.valueOf("2002-01-29"),new String[]{"Bacc","License","Master"}));
        return list_emp;
    }

    @WebUrl(url = "save-emp.do")
    public ModelView save(){
        list_emp.add(this);
        return list();
    }

    @WebUrl(url = "list-emp.do")
    public ModelView list(){
        ModelView mv=new ModelView();
        mv.setUrl_direction("list_emp.jsp");
        mv.putData("emps",list_emp);
        return mv;
    }

    @Override
    public String toString() {
        return "Employe{" +
                "nom='" + nom + '\'' +
                ", prenoms='" + prenoms + '\'' +
                ", dateNaissance=" + dateNaissance +
                ", diplomes=" + Arrays.toString(diplomes) +
                '}';
    }
}
