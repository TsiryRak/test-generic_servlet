<%@ page import="genericDAO.Model" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="models.Employe" %>
<%@ page import="java.util.Arrays" %><%--
  Created by IntelliJ IDEA.
  User: tsiry
  Date: 18/11/2022
  Time: 15:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    ArrayList<Employe> employes = (ArrayList<Employe>) request.getAttribute("emps");
%>
<html>
<head>
    <title>Employe</title>
</head>
<body>
<h1>Liste des employés</h1>
<table border="1">
    <thead>
    <tr>
        <th>Nom</th>
        <th>Prénom(s)</th>
        <th>Date de naisance</th>
        <th>Diplômes</th>
    </tr>
    </thead>
    <tbody>
    <%
        for (Employe emp : employes) {
    %>
    <tr>
        <td><%= emp.getNom() %></td>
        <td><%= emp.getPrenoms() %></td>
        <td><%= emp.getDateNaissance().toString() %></td>
        <td><%= Arrays.toString(emp.getDiplomes()) %></td>
    </tr>
    <%
        }
    %>
    </tbody>
</table>
</body>
</html>
