<%--
  Created by IntelliJ IDEA.
  User: tsiry
  Date: 18/11/2022
  Time: 15:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employe</title>
</head>
<body>
<form action="save-emp.do" method="post">
    <h1>Insérer un nouvel employé</h1>
    <table>
        <tr>
            <td><label for="nom">Nom</label></td>
            <td><input id="nom" type="text" name="nom"></td>
        </tr>
        <tr>
            <td><label for="prenoms">Prénoms</label></td>
            <td><input id="prenoms" type="text" name="prenoms"></td>
        </tr>
        <tr>
            <td><label for="dateNaissance">Date de naissance</label></td>
            <td>
                <input id="dateNaissance" type="date" name="datenaissance">
            </td>
        </tr>
        <tr>
            <td><label for="dipl">Diplomes</label></td>
            <td id="dipl">
                <input id="dipl0" type="checkbox" value="Bacc" name="diplomes"><label for="dipl0">Bacc</label>
                <input id="dipl1" type="checkbox" value="License" name="diplomes"><label for="dipl1">License</label>
                <input id="dipl2" type="checkbox" value="Master" name="diplomes"><label for="dipl2">Master</label>
                <input id="dipl3" type="checkbox" value="Doctorat" name="diplomes"><label for="dipl3">Doctorat</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Valider"></td>
        </tr>
    </table>
</form>
</body>
</html>
